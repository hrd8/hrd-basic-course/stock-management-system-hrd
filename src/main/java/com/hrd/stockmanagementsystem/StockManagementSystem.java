package com.hrd.stockmanagementsystem;

import com.hrd.stockmanagementsystem.model.Product;

import java.util.ArrayList;
import java.util.List;

import static com.hrd.stockmanagementsystem.services.Service.load;
import static com.hrd.stockmanagementsystem.ui.Ui.*;

public class StockManagementSystem {
    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>();
        welcome();
        load(productList,false);
        displayMenu(productList);
    }
}
