package com.hrd.stockmanagementsystem.model;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class Product implements Serializable {
    private static int count = 1;
    private int id;
    private String name;
    private double unitPrice;
    private int qty;
    private final LocalDate importedDate;

    public Product(String name, double unitPrice, int qty) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.id = count++;
        this.importedDate = LocalDate.now();
    }

    public Product(int id, String name, double unitPrice, int qty, LocalDate importedDate) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.importedDate = importedDate;
    }

    public String[] getProductArray(){
        return new String[]{String.valueOf(this.getId()), this.getName(), String.valueOf(this.getUnitPrice()), String.valueOf(this.getQty()), this.getImportedDate().toString()};
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return this.id+","+this.name+","+this.unitPrice+","+this.qty+","+this.importedDate;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public LocalDate getImportedDate() {
        return importedDate;
    }
    public void setProduct(String name, double unitPrice, int qty){
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
    }
    public static void setCount(int counter){
        count = counter;
    }
    public static void decreaseCounter(){
        count--;
    }
}
