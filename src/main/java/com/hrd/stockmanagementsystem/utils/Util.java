package com.hrd.stockmanagementsystem.utils;

import com.hrd.stockmanagementsystem.model.Product;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import static java.lang.Thread.sleep;
public class Util {
    public static void printWithThread(String string,int delay,boolean threadEnd) throws InterruptedException {
        Runnable runnable =
                () ->
                        string
                                .chars()
                                .mapToObj(character ->(char)character)
                                .forEach(
                                        character -> {
                                            try {
                                                sleep(delay);
                                            } catch (InterruptedException e) {
                                                System.err.println(e.getMessage());
                                            }
                                            System.out.print(character);
                                        });
        Thread thread = new Thread(runnable);
        thread.start();
        if (!threadEnd){
            thread.join();
        }
    }
    public static int getInputInt()throws Exception{
        int number;
        Scanner scanner = new Scanner(System.in);
        while (true){
            number  = scanner.nextInt();
            if (number<0){
                    return -1;
            }
            break;
        }
        return number;
    }
    public static double getInputDouble()throws Exception{
        double number;
        Scanner scanner = new Scanner(System.in);
        while (true){
            number  = scanner.nextDouble();
            if (number<0){
                return -1;
            }
            break;
        }
        return number;
    }
    public static String getTableLayout(String start,String middle,String end,int displayTextLength){
        String layoutString = "";
        for (int i=0;i<displayTextLength;i++){
            if (i==0){
                layoutString+=start;
            }else if (i==displayTextLength-1){
                layoutString+=end;
            }else {
                layoutString+=middle;
            }
        }
        return layoutString;
    }
    public static void writeToFile(List<Product> productList, BufferedWriter bufferedWriter) throws IOException {
        long start = System.currentTimeMillis();
        for (Product product: productList) {
            bufferedWriter.write(product.toString());
            bufferedWriter.write(System.getProperty( "line.separator" ));
        }
        bufferedWriter.close();
        long end = System.currentTimeMillis();
        System.out.println((end - start) / 1000f + " seconds");
    }
    //    Method which will be used when need to confirm answer from user
    public static boolean confirmAnswer () {
        Scanner input = new Scanner(System.in);
        String answer = input.next();
        while (true) {
            if (answer.equalsIgnoreCase("Y")) {
                return true;
            } else if (answer.equalsIgnoreCase("N")) {
                return false;
            } else {
                continue;
            }
        }
    }

}
